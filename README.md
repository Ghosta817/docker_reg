## Локальный docker registry.
    

### Задача.

С помощью vagrant развернута одна VM, на ней установлены:  
```bash    
docker
docker-compose
openssl
gitlab-runner
```
Так же с помощью docker-compose развернуты два контейнера:  
```bash
http:2.4
registry:2
```
SSL-сертификат получен самоподписной с помошью openssl.   
Настроен basic-auth для реджистри, используя образ http:2.4, он же используется в качестве фронтэнда для реджестри.   
Автоматизация настроена в двух вариантах: через vagrant provisioning (test.sh), а так же в gitlab-ci (push нового образа в наш реджестри).   
