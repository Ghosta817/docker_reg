#!/bin/bash

echo "---------------############ test.sh ############---------------"
docker pull alpine:latest
docker login $DR_IP:5043 -u $DR_USER -p $DR_PASS
docker tag alpine $DR_IP:5043/alpine_test
docker push $DR_IP:5043/alpine_test