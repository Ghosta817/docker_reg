#!/bin/bash

#Install gitlab-runner
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner
sleep 10

#Register gitlab-runner
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token $PROJECT_REGISTRATION_TOKEN \
  --executor "shell" \
  --description "shell-runner" \
  --maintenance-note "Free-form maintainer notes about this runner" \
  --tag-list "shell" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"

#Add gitlab-runner to docker group
usermod -aG docker gitlab-runner
newgrp docker

#Verifying runner...
gitlab-runner verify